set termguicolors
set t_Co=256
set clipboard+=unnamedplus

call plug#begin('~/.local/share/nvim/plugged')

Plug 'itchyny/lightline.vim'
Plug 'norcalli/nvim-colorizer.lua'
Plug 'dracula/vim', { 'as': 'dracula' }

call plug#end()

lua require'colorizer'.setup()
