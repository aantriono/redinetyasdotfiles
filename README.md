### My personal dotfiles  
I accidentally remove all of my .config files in my laptop and it's so painful even my own dotfiles contain nothing compared to other user.  
  
## My application list  
  
# Sway  
OS = [Langitketujuh](https://langitketujuh.id)  
Terminal = kitty  
Application launcher = wofi  
Screenshot = grim, swappy, slurp  
Brightness controller = brightnessctl  
Audio server = pipewire, wireplumber  
Player control = playerctl  
Wallpaper = azote  
  
# Waybar  
[Status waktu shalat](https://gitlab.com/nesstero/status-waktu-shalat)  
  
## Screenshot
![Screenshot](Screenshot.png)
